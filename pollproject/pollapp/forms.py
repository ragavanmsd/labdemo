from django import forms
from pollapp.models import Poll

class createPollForm(forms.ModelForm):
    class Meta():
        model = Poll
        fields=['question','option_one','option_two','option_three']

