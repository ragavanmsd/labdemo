from django.shortcuts import render,redirect
from pollapp.forms import createPollForm
from pollapp.models import Poll
from django.http import HttpResponse

# Create your views here.

def home(request):

    polls=Poll.objects.all()

    context={ 'polls' : polls}

    return render(request,'htmlfiles/home.html',context)

def create(request):

    if request.method == 'POST':
        form = createPollForm(data= request.POST)
        if form.is_valid():
            print(form.cleaned_data['question'])
            form.save()
            return redirect('home')
    else:
        form=createPollForm()

    context={ 
        'form' : form 
        }

    return render(request,'htmlfiles/create.html',context)
    

def votes(request,poll_id):
    poll=Poll.objects.get(pk=poll_id)

    if request.method=='POST':
        selected_option= request.POST['poll']

        if selected_option == 'option1':
            poll.option_one_count+=1
        elif selected_option == 'option2':
            poll.option_two_count+=1
        elif selected_option == 'option3':
            poll.option_three_count+=1
        else:
            return HttpResponse(400,'Invalid form') 
        poll.save()

        return redirect('results',poll.id)

    context={
        'poll':poll
    }

    return render(request,'htmlfiles/votes.html',context)
    

def results(request,poll_id):
    poll=Poll.objects.get(pk=poll_id)
    context={
        'poll':poll
    }
    

    return render(request,'htmlfiles/results.html',context)

