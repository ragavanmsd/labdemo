from django.contrib import admin
from pollapp.models import Poll

# Register your models here.
admin.site.register(Poll)
